
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// define schema
var PaymentSchema = new Schema({
    salary: Number,
    title: String
});

module.exports = mongoose.model('Payment', PaymentSchema);

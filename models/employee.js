
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');

// define schema
var EmployeeSchema = new Schema({
    number: Number,
    name: String,
    title: [{type: Schema.ObjectId, ref: "Payment"}],
});

autoIncrement.initialize(mongoose.connection);
EmployeeSchema.plugin(autoIncrement.plugin, {model:'Employee',field:'number'});


module.exports = mongoose.model('Employee', EmployeeSchema);

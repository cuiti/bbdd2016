var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');

var ProjectSchema = new Schema({
    number: Number,
    name: String,
    budget: Number,
});

autoIncrement.initialize(mongoose.connection);

ProjectSchema.plugin(autoIncrement.plugin, {model:'Project',field:'number'});

module.exports = mongoose.model('Project', ProjectSchema);

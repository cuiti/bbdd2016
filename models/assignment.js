var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AssignmentSchema = new Schema({
    employee: [{type: Schema.ObjectId, ref: "Employee"}],
    project: [{type: Schema.ObjectId, ref: "Project"}],
	responsibility: String,
	duration: Number 
});

module.exports = mongoose.model('Assignment', AssignmentSchema);

var Project = require('../models/project');

//requerir el loger
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

//requerir el helper de seguridad
var securityController = require('../utils/securityController');
var sanitize = new securityController();

var bodyParser = require('body-parser');
var app = require('express')();

//Métodos GET para /api/projects
exports.listProjects = function (req, res, next) {
	Project
		.find(function (err, proy) {
        if (err) {
            logger.error('Error al intentar recuperar todos los proyectos');
            logger.error('El error es: ' + err);
            return next(err);
        }
        res.json({message: "Los proyectos son: ", projects: proy});
		})
		.populate('title');
};

exports.getProject = function (req, res, next) {

    Project.findOne({number: req.params.number}, function (err, project) {
        if (err) {
            logger.error('Error al buscar un proyecto con id: ' + req.params.number + ' el error es:');
            logger.error(err);
            return next(err);
        }
        if (project != undefined) {
            return res.json({message: "El project es: ", p: project});
        } else {
            return res.json({message: 'No existe ese proyecto'});
        }
    });
};

// Método POST para /api/projects
exports.createProject = function (req, res, next) {
	
	app.use(bodyParser.urlencoded({ extended: true }));	//para parsear el body y agarrar los parámetros que trae

	
    var proy = new Project();
	
    if (req.body.name && req.body.budget){
		//logger.info("creating project : " + req.body.name);

		proy.name = req.body.name;
		proy.budget = req.body.budget;

		proy.save(function (err) {
			if (err) {
				logger.error('Error al añadir el proyecto. El error es:');
				logger.error(err);
				return next(err);
			}
			logger.info('proyecto  '+proy.name+' añadido con éxito');
			return res.json({message: 'project added', proy: proy});
		});

	}else{
		logger.info("parameters missing");
		res.status(422);
		return res.json({message: 'missing parameter(s): name, number and budget are required'});
	}
};

//Método DELETE de /api/projects
exports.deleteProject = function (req, res, next) {
    logger.info("llego un DELETE para el proyecto: "+req.params.number)
    Project.remove({number: req.params.number},
        function (err, app) {
            if (err) {
                logger.error('Error al intentar borrar el proyecto: ' + req.params.number, ' el error es:');
                logger.error(err);
                return next(err);
            }
            logger.info('El proyecto: ' + req.params.number + ' fue eliminado exitosamente');
            res.json({message: 'Eliminacion exitosa'});
        });
};

//Método PUT
exports.updateProject = function (req, res, next) {

    Project.findOne({number: req.params.number},
        function (err, project) {
            if (err) {
                logger.error('Error al actualizar el proyecto: ' + req.params.number + 'el error es:');
                logger.error(err);
                return next(err);
            }
			
			if (project != null){	//si el proyecto existe
				app.use(bodyParser.urlencoded({ extended: true }));
				
				//chequeo cuales parametros se envian para modificar
				if (req.body.name) { project.name = req.body.name }
				//if (req.body.number) {project.number = req.body.number}
				if (req.body.budget){project.budget = req.body.budget}

				project.save(function (err) {
					if (err) {
						return res.json(err);
					}
					logger.info('Se actualizó el proyecto con numero: ' + req.params.number);
					res.json({message: 'project updated ', proyecto: project});
				});

				
			}else{
				logger.info('Intentando actualizar el proyecto: ' + req.params.number + ' no se encuentra');
				res.json({message: 'project number not found'});
			}
        });
};
//requerir los documentos
var Employee = require('../models/employee');
var Assignment = require('../models/assignment');
var Project = require('../models/project');

//requerir el loger
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

//requerir el helper de seguridad
var securityController = require('../utils/securityController');
var sanitize = new securityController();

var bodyParser = require('body-parser');
var app = require('express')();

//Métodos GET
exports.listAssignments = function (req, res, next) {
	Assignment
		.find(function (err, asig) {
        if (err) {
            logger.error('Error al intentar recuperar todas las asignaciones');
            logger.error('El error es: ' + err);
            return next(err);
        }
        res.json({message: "Las asignaciones son: ", assignments: asig});
		})
		.populate('employee')
		.populate('project');
};

exports.getAssignment = function (req, res, next) {

	Assignment.findOne({_id: req.params.id}, function (err, assignment) {
        if (err) {
            logger.error('Error getting Assignment: ' + req.params.id + ' error:');
            logger.error(err);
            return next(err);
        }
        if (assignment != undefined) {
            return res.json({message: "Assignment: ", asig: assignment});
        } else {
			res.code(404);
            return res.json({message: 'Unexistent assignment'});
        }
    });
};


// Método POST para /api/employees
exports.createAssignment = function (req, res, next) {
	
	app.use(bodyParser.urlencoded({ extended: true }));	//para parsear el body y agarrar los parámetros que trae

    var assig = new Assignment();

	if (req.body.responsibility && req.body.duration && req.body.employee_num && req.body.project_num) {
		assig.responsibility = req.body.responsibility;
		assig.duration = req.body.duration;
	}else{
		logger.info("parameters missing");
		res.status(422);
		return res.json({message: 'missing parameter(s): project_num, employee_num, duration and responsibility are required'});
	}
	
	var emp = Employee.findOne({ 'number': req.body.employee_num });	//busca el empleado
	emp.exec(function (err, employee) {
		if (err) {
			return res.json({message: 'error while getting employee'+req.body.employee_num});}
		else{
			if (employee != null){
				assig.employee = employee._id;
				//logger.info('employee found: '+employee.name);

				var proj = Project.findOne({ 'number': req.body.project_num });	//dado que el empleado existe, busca el proyecto
				proj.exec(function (err, project) {
					if (err) {
						return res.json({message: 'Error getting project '});}
					else{
						if (project != null){
							assig.project = project._id;
							//logger.info('project found: '+project.name);

							assig.save(function (err) {
								if (err) {
									logger.error('Error al añadir assig. El error es:');
									logger.error(err);
									return next(err);
								}
								logger.info('new assignment saved');
								return res.json({message: 'assignment saved', assignment: assig});
							});
						}else{
							res.status(404);
							return res.json({message: 'project unexistent: '+req.body.project_num});
						}

					}
				});
			}
			else{
				res.code(404);
				return res.json({message: 'el empleado '+req.body.employee_num+'no existe'});
			}
	
		}
	})


};

//Método DELETE
exports.deleteAssignment = function (req, res, next) {
	Assignment.remove({_id: req.params.id},
        function (err, app) {
            if (err) {
                logger.error('Error al intentar borrar: ' + req.params.id, ' el error es:');
                logger.error(err);
                return next(err);
            }
            logger.info(req.params.id + ' fue eliminado exitosamente');
            res.json({message: 'Eliminacion exitosa'});
        });
};

//Método PUT

exports.updateAssignment = function (req, res, next) {

	Assignment.findOne({_id: req.params.id},
        function (err, assig) {
            if (err) {
                logger.error('Error al actualizar: ' + req.params.id + 'el error es:');
                logger.error(err);
                return next(err);
            }
			
			if (assig != null){	//si el asig existe
				app.use(bodyParser.urlencoded({ extended: true }));
				
				//chequeo cuales parametros se envian para modificar
				if (req.body.duration) { assig.duration = req.body.duration }
				if (req.body.responsibility) {assig.responsibility = req.body.responsibility}
				if (req.body.employee_num) {
					var emp = Employee.findOne({ 'number': req.body.employee_num });	//busca el empleado
					emp.exec(function (err, employee) {
						if (err) {
							return res.json({message: 'error getting employee'+req.body.employee_num});}
						else{
							if (employee != null){
								assig.employee = employee._id;
								logger.info('employee found: '+employee.name);

								if (req.body.project_num){
									var proj = Project.findOne({ 'number': req.body.project_num });
									proj.exec(function (err, project) {
										if (err) {
											return res.json({message: 'Error getting project '});}
										else{
											if (project != null){
												assig.project = project._id;
												logger.info('project found: '+project.name);

												assig.save(function (err) {
													if (err) {
														logger.error('Error al añadir assig. El error es:');
														logger.error(err);
														return next(err);
													}
													return res.json({message: 'assignment saved', assignment: assig});
												});
											}else{
												return res.json({message: 'project unexistent: '+req.body.project_num});
											}

										}
									});
								}
								else{
									assig.save(function (err) {
										if (err) {
											return res.json(err);
										}
										res.json({message: 'assignment updated ', emp: assig});
									});
								}
							}
							else{
								res.status(404);
								return res.json({message: 'el empleado '+req.body.employee_num+'no existe'});
							}

						}
					})
		
				}
				else{
					if (req.body.project_num){
						var proj = Project.findOne({ 'number': req.body.project_num });
						proj.exec(function (err, project) {
							if (err) {
								return res.json({message: 'Error getting project '});}
							else{
								if (project != null){
									assig.project = project._id;
									logger.info('project found: '+project.name);

									assig.save(function (err) {
										if (err) {
											logger.error('Error al añadir assig. El error es:');
											logger.error(err);
											return next(err);
										}
										return res.json({message: 'assignment saved', assignment: assig});
									});
								}else{
									return res.json({message: 'project unexistent: '+req.body.project_num});
								}

							}
						});
					}else{
						assig.save(function (err) {
							if (err) {
								return res.json(err);
							}
							logger.info('Se actualizó la asignacion');
							res.json({message: 'assignment updated ', emp: assig});
						});
					}

				}
				
			}else{
				logger.info('Intentando actualizar la asignacion: ' + req.params.id + ' no se encuentra');
				res.json({message: 'assignment number not found'});
			}
        });
};
//requerir el documento
var Payment = require('../models/payment');

//requerir el loger
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

//requerir el helper de seguridad
var securityController = require('../utils/securityController');
var sanitize = new securityController();

var bodyParser = require('body-parser');
var app = require('express')();

//Métodos GET para /api/payments
exports.listPayments = function (req, res, next) {
    Payment.find(function (err, pay) {
        if (err) {
            logger.error('El error es: ' + err);
            return next(err);
        }
        res.json({message: "Los titulos y salarios son: ", payments: pay});
    });
};



// Método POST para /api/payments
exports.createPayment = function (req, res, next) {
	
	app.use(bodyParser.urlencoded({ extended: true }));	//para parsear el body y agarrar los parámetros que trae
	
    var pay = new Payment();

    if (req.body.title && req.body.salary) {
        pay.title = req.body.title;
        pay.salary = req.body.salary;
        pay.save(function (err) {
            if (err) {
                logger.error('Error al añadir el titulo. El error es:');
                logger.error(err);
                return next(err);
            }
            logger.info(pay.title+' añadido con éxito');
            return res.json({message: 'titulo añadido', pay: pay});
        });

    } else{
        logger.info("POST payment: parameters missing");
        res.status(422);
        return res.json({message: 'missing parameter(s): title and salary are required'});
    }



};

//Método DELETE de /api/payments
exports.deletePayment = function (req, res, next) {
    logger.info("llego un DELETE para el id: "+req.params.id)
    Payment.remove({_id: req.params.id},
        function (err, app) {
            if (err) {
                logger.error('Error al intentar borrar el titulo: ' + req.params.id, ' el error es:');
                logger.error(err);
                return next(err);
            }
            logger.info(' ' + req.params.id + ' fue eliminado exitosamente');
            res.json({message: 'Eliminacion exitosa'});
        });
};
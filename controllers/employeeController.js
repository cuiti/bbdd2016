//requerir los documentos
var Employee = require('../models/employee');
var Payment = require('../models/payment');
var Assignment = require('../models/assignment');

//requerir el loger
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

//requerir el helper de seguridad
var securityController = require('../utils/securityController');
var sanitize = new securityController();

var bodyParser = require('body-parser');
var app = require('express')();

//Métodos GET para /api/employees
exports.listEmployees = function (req, res, next) {
    Employee
		.find(function (err, emp) {
        if (err) {
            logger.error('Error al intentar recuperar todos los empleados');
            logger.error('El error es: ' + err);
            return next(err);
        }
        res.json({message: "Los empleados son: ", employees: emp});
		})
		.populate('title');
};

exports.getEmployee = function (req, res, next) {
    Employee.findOne({number: req.params.number}, function (err, employee) {
        if (err) {
            logger.error('Error al buscar un empleado con id: ' + req.params.number + ' el error es:');
            logger.error(err);
            return next(err);
        }
        if (employee != undefined) {
            return res.json({message: "El employee es: ", employee: employee});
        } else {
			res.status(404);
            return res.json({message: 'No existe ese empleado'});
        }
    });
};

// employees/:number/projects
exports.getProjectsForEmployee = function(req, res, next){
	Employee.findOne({number: req.params.number}, function (err, employee) {
		if (err) {
			logger.error('Error al buscar un empleado con numero: ' + req.params.number + ' el error es:');
			logger.error(err);
			return next(err);
		}
		if (employee != undefined) {

			Assignment.find(function (err, asig) {
					if (err) {
						logger.error('Error al intentar recuperar todas las asignaciones');
						logger.error('El error es: ' + err);
						return next(err);
					}
					res.json({message: "Employee "+req.params.number+"'s projects: ", assignments: asig});
				})
				.populate('employee')
				.populate('project');
		} else {
			res.status(404);
			return res.json({message: 'No existe ese empleado'});
		}
	});
}

// Método POST para /api/employees
exports.createEmployee = function (req, res, next) {
	
	app.use(bodyParser.urlencoded({ extended: true }));	//para parsear el body y agarrar los parámetros que trae
	//logger.info("agregando el empleado: " + req.body.name);
	
    var emp = new Employee();
	if (req.body.name && req.body.title) {
		emp.name = req.body.name;
		var pay = Payment.findOne({ 'title': req.body.title });
		pay.exec(function (err, payment) {
			if (err) {
				return res.json({message: 'title '+req.body.title+' does not exist'});}
			else{
				if (payment != null){
					emp.title = payment._id;
					//logger.info('payment found: '+payment.title + ' ' +payment.salary);

					emp.save(function (err) {
						if (err) {
							logger.error('Error adding employee:');
							logger.error(err);
							return next(err);
						}
						logger.info('empleado '+emp.name+' añadido con éxito');
						return res.json({message: 'employee added'});
					});
				}
				else{
					res.status(404);
					return res.json({message: 'title '+req.body.title+' does not exist'});
				}


			}
		});
	}
	else{
		res.status(422);
		return res.json({message: 'parameters missing: name and title are required to create an employee'});}



};

//Método DELETE de /api/employees
exports.deleteEmployee = function (req, res, next) {
    logger.info("llego un DELETE para el empleado: "+req.params.id)
    Employee.remove({number: req.params.id},
        function (err, app) {
            if (err) {
                logger.error('Error al intentar borrar el empleado: ' + req.params.id, ' el error es:');
                logger.error(err);
                return next(err);
            }
            logger.info('El empleado: ' + req.params.id + ' fue eliminado exitosamente');
            res.json({message: 'Employee ' + req.params.id + ' deleted'});
        });
};

//Método PUT para /api/employees
exports.updateEmployee = function (req, res, next) {

    Employee.findOne({number: req.params.id},
        function (err, emp) {
            if (err) {
                logger.error('Error al actualizar el empleado: ' + req.params.id + 'el error es:');
                logger.error(err);
                return next(err);
            }
			
			if (emp != null){	//si el empleado existe
				app.use(bodyParser.urlencoded({ extended: true }));
				
				//chequeo cuales parametros se envian para modificar
				if (req.body.name) { emp.name = req.body.name }	
				if (req.body.number) {emp.number = req.body.number}
				if (req.body.title) {
					//si se quiere cambiar el titulo, guardo el empleado en el callback de busqueda del titulo
					var pay = Payment.findOne({ 'title': req.body.title });
					pay.exec(function (err, payment) {	
						if (err) {
							return res.json({message: 'Error al buscar el titulo '});}
						else{
							if (payment != null){
								emp.title = payment._id;
								logger.info('payment found: '+payment.title + ' ' +payment.salary);
								
								emp.save(function (err) {
									if (err) {
										logger.error('Error al añadir al empleado. El error es:');
										logger.error(err);
										return next(err);
									}
									logger.info('empleado '+emp.name+' guardado con éxito');
									return res.json({message: 'empleado guardado', emp: emp});
								});
							}else{
								res.status(404);
								return res.json({message: 'el titulo '+req.body.title+' no existe'});
							}
					
						}
					});
		
				}
				else{
					//si no se cambia el titulo, guardo normalmente
					emp.save(function (err) {
					if (err) {
						return res.json(err);
					}
					logger.info('Se actualizó el empleado con numero: ' + req.params.id);
					res.json({message: 'employee updated ', emp: emp});
				});
				}
				
			}else{
				logger.info('Intentando actualizar el empleado: ' + req.params.id + ' no se encuentra');
				res.json({message: 'employee number not found'});
			}
        });
};



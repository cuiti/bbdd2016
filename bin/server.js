//Load require packages
var express = require('express');
var mongoose = require('mongoose');
var config = require('../config');
var bodyParser = require('body-parser');
var passport = require('passport');
var autoIncrement = require('mongoose-auto-increment');


var app = express(); //Create the Express app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(passport.initialize());

//routes are defined here
var employees = require('../routes/employees');
var payments = require('../routes/payments');
var projects = require('../routes/projects');
var assignments = require('../routes/assignments');

var urlMongoose = config.Mongo.client + "://" + config.Mongo.host + ":" + config.Mongo.port + "/" + config.Mongo.dbName;
var urlNodo5 = config.Mongo.client + "://10.0.2.5:" + config.Mongo.port + "/" + config.Mongo.dbName;
var urlNodo6 = config.Mongo.client + "://10.0.2.6:" + config.Mongo.port + "/" + config.Mongo.dbName;

mongoose.connect(urlMongoose, function (err) {
   if (err){
		console.log("error conectando a primera bdd, intentando con nodo5");
		mongoose.connect(urlNodo5, function (err) {
		    if (err){
				console.log("error conectando a nodo5, intentando con nodo6");
				mongoose.connect(urlNodo6, function (err) {
				    if (err) throw err;
				});
	}
		});
	}
});

var db = mongoose.connection;
autoIncrement.initialize(db);

var port = process.env.PORT || 8888;

// Create our Express router
var router = express.Router();

// Initial dummy route for testing
router.get('/', function (req, res) {
    res.json({message: 'App Working!'});
});

// Register all our routes with /api
app.use('/api', router);
app.use('/api/employees', employees);
app.use('/api/payments', payments);
app.use('/api/projects', projects);
app.use('/api/assignments', assignments);

//error handler para todas las rutas
app.use(function (err, req, res, next) {
    if (err) {
        console.log(err);
        res.status(500).send(err);
    }
});

// Start the server
app.listen(port);

exports.app = app;


console.log('Express is running in port: ' + port);

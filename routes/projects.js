var projectController = require('../controllers/projectController');
var express = require('express');
var router = express.Router();

router.route('/')
    .get(projectController.listProjects)
    .post(projectController.createProject);

router.route('/:number')
    .get(projectController.getProject)
    .delete(projectController.deleteProject)
    .put(projectController.updateProject);
	
module.exports = router;
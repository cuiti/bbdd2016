var assignmentController = require('../controllers/assignmentController');
var express = require('express');
var router = express.Router();


router.route('/')
    .get(assignmentController.listAssignments)
    .post(assignmentController.createAssignment);

 router.route('/:id')
    .get(assignmentController.getAssignment)
    .delete(assignmentController.deleteAssignment)
    .put(assignmentController.updateAssignment);

module.exports = router;
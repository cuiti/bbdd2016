var paymentController = require('../controllers/paymentController');
var express = require('express');
var router = express.Router();

router.route('/')
    .get(paymentController.listPayments)
    .post(paymentController.createPayment);

router.route('/:id')
    .delete(paymentController.deletePayment);
	
module.exports = router;
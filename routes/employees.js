var employeeController = require('../controllers/employeeController');
var express = require('express');
var router = express.Router();


// '/api/employees'
router.route('/')
    .get(employeeController.listEmployees)
    .post(employeeController.createEmployee);

//'/api/employees/:id'
 router.route('/:number')
    .get(employeeController.getEmployee)
    .delete(employeeController.deleteEmployee)
    .put(employeeController.updateEmployee);


router.route('/:number/projects')
    .get(employeeController.getProjectsForEmployee);

module.exports = router;